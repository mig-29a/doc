# Hello!

This is an incremental record of my activities in the field of information technology, computer science, and software engineering.

## License

Copyright (c) 2018 MiG-29A \<mig-29a@protonmail.com\>. The content of this repository is licensed under [the MIT License](LICENSE).

